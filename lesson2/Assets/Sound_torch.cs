﻿using System.Collections;
using System.Collections.Generic;
using FMOD.Studio;
using UnityEngine;

public class Sound_torch : MonoBehaviour
{
    [FMODUnity.EventRef] public string torchevent;
    FMOD.Studio.EventInstance torchinstance;
    
    // Start is called before the first frame update
    void Start()
    {
        torchinstance = FMODUnity.RuntimeManager.CreateInstance(torchevent);
        //torchinstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(torchinstance, gameObject.GetComponent<Transform>(), gameObject.GetComponent<Rigidbody>() );
        torchinstance.start();
        
        //FMODUnity.RuntimeManager.PlayOneShot(torchevent, gameObject.transform.position);


    }

    // Update is called once per frame
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            torchinstance.stop(STOP_MODE.ALLOWFADEOUT);
        }

    }
}
