﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;


public class Sound_Footstep : MonoBehaviour
{
    [FMODUnity.EventRef] public string footstepsevent;
    public vThirdPersonInput vikingInput;
    
    
    // Start is called before the first frame update
    void Start()
    {
        vikingInput = GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void footsteps()
    {
        if (vikingInput.cc.inputMagnitude > 0.1)
        FMODUnity.RuntimeManager.PlayOneShotAttached(footstepsevent, gameObject);
    }
}
