﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;


public class Sound_Land : MonoBehaviour

{
    [FMODUnity.EventRef] public string landevent;
    public vThirdPersonInput vikingInput;
    

    // Start is called before the first frame update
    void Start()
    {
        vikingInput = GetComponent<vThirdPersonInput>();
    }

    void land()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(landevent, gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
