﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class Sound_Jump : MonoBehaviour
{
    [FMODUnity.EventRef] public string jumpevent;

    public vThirdPersonInput vikingInput;
    
    // Start is called before the first frame update
    void Start()
    { 
        vikingInput = GetComponent<vThirdPersonInput>();
    }
    
    void jump() 

    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(jumpevent, gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
