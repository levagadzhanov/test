using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Invector.vCharacterController;

public class footsteps_surface : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string footstepsevent;

    public vThirdPersonInput tpInput;
    public LayerMask sloy;
    public float tipPoverhnosti;
    
    // Start is called before the first frame update
    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void footsteps()
    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            
            
            
            ProverkaPoverhnosti();
            FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(footstepsevent);
            eventInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform.position));
            eventInstance.setParameterByName("isRunning", tpInput.cc.inputMagnitude);
            eventInstance.setParameterByName("surface_type", tipPoverhnosti);
            eventInstance.start();
            eventInstance.release();
        }
        
    }

    void ProverkaPoverhnosti()
    {
        RaycastHit luch;
        if (Physics.Raycast(transform.position, Vector3.down, out luch, 0.3f, sloy))
        {
            Debug.Log(luch.collider.tag);
            if (luch.collider.CompareTag("vDirt")) tipPoverhnosti = 0;
            else if (luch.collider.CompareTag("vSnow")) tipPoverhnosti = 1;
            else if (luch.collider.CompareTag("vWater")) tipPoverhnosti = 2;
            else if (luch.collider.CompareTag("vWood")) tipPoverhnosti = 3;
            else tipPoverhnosti = 1;
        }
        
        
    }
}
