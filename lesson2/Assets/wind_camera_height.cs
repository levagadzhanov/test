﻿using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;

public class wind_camera_height : MonoBehaviour
{
    public GameObject camera;

    [FMODUnity.EventRef] public string windevent;

    private FMOD.Studio.EventInstance windinstance;
    
    public DayNightController dnc;
    // public GameObject daynight;
    
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        windinstance = FMODUnity.RuntimeManager.CreateInstance(windevent);
        windinstance.start();
        //dnc = daynight.GetComponent<DayNightController>();


    }

    // Update is called once per frame
    void Update()
    {
        windinstance.setParameterByName("camera_height", camera.transform.position.y);
        // windinstance.setParameterByName("daynight", dnc.currentTime);
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("daynight", dnc.currentTime);
        Debug.Log(dnc.currentTime);
        


    }
}
